import React from "react";
import WeatherCard from "../../components/WeatherCard/WeatherCard";
import "./WeatherApp.scss";
import { connect } from "react-redux";

function WeatherApp(props) {
  return (
    <div className="weatherWrapper">
      <WeatherCard
        address={props.data.address}
        isCur={true}
        target={props.target}
        id="0000"
        active={true}
        card={props.data.current}
      />
      {props.data.daily.map((e, index) => (
        <WeatherCard
          address={props.data.address}
          key={index + 1}
          target={props.target}
          id={"000" + (index + 1)}
          active={false}
          card={e}
        />
      ))}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    target: state.WeatherApp.target,
  };
};
export default connect(mapStateToProps, null)(WeatherApp);
