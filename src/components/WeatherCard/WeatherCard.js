import React from "react";
import "./WeatherCard.scss";
import { getIconClassName, timeFormat } from "../../helpers";

const WeatherCard = (props) => {
  console.log(props.address);
  const date = new Date(props.card.dt * 1000);
  const days = [
    "Воскресенье",
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
  ];
  const curTime = timeFormat(date);
  const sunrise = timeFormat(new Date(props.card.sunrise * 1000));
  const sunset = timeFormat(new Date(props.card.sunset * 1000));
  const iconClass = getIconClassName(props.card.weather[0].main);
  return (
    <div>
      {props.target === props.id ? (
        <div id={props.id} className="weatherCard">
          <h3>{days[date.getDay()]}</h3>
          {props.isCur ? (
            <h4>{curTime}</h4>
          ) : (
            <h4>{date.toLocaleDateString()}</h4>
          )}
          <h3>
            {props.address.city} / {props.address.country}
          </h3>
          <div className={`weather-icon ${iconClass}`}></div>
          <div>
            {props.isCur ? (
              <h3>
                Температура: {Math.round(props.card.temp)}
                <sup>o</sup>C
              </h3>
            ) : (
              <div className="tempWrap">
                <h3>Температура</h3>
                <p>
                  Утром: {Math.round(props.card.temp.morn)}
                  <sup>o</sup>C Ощущается как:
                  {Math.round(props.card.feels_like.morn)}
                  <sup>o</sup>C
                </p>
                <p>
                  Днём: {Math.round(props.card.temp.day)}
                  <sup>o</sup>C Ощущается как:
                  {Math.round(props.card.feels_like.day)}
                  <sup>o</sup>C
                </p>
                <p>
                  Вечером: {Math.round(props.card.temp.eve)}
                  <sup>o</sup>C Ощущается как:
                  {Math.round(props.card.feels_like.eve)}
                  <sup>o</sup>C
                </p>
                <p>
                  Ночью: {Math.round(props.card.temp.night)}
                  <sup>o</sup>C Ощущается как:
                  {Math.round(props.card.feels_like.night)}
                  <sup>o</sup>C
                </p>
              </div>
            )}

            <p>Восход: {sunrise}</p>
            <p>Закат: {sunset}</p>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default WeatherCard;
