import React from "react";
import "./AppHeader.scss";
import { Layout, Menu } from "antd";
import { WeatherAppOperations } from "../../store/WeatherApp";
import { useDispatch } from "react-redux";
const { Header } = Layout;

function AppHeader(props) {
  const dispatch = useDispatch();
  const targetHandler = (e) => {
    const id = e.domEvent.target.id;
    dispatch(WeatherAppOperations.setTarget(id));
  };
  return (
    <Layout className="layout">
      <Header className="head-bar">
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["0"]}>
          <Menu.Item id="0000" key="0" onClick={targetHandler}>
            <span id="0000">
              Текущая <br /> Погода
            </span>
          </Menu.Item>
          {props.data.daily.map((el, index) => {
            const days = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];
            const date = new Date(el.dt * 1000);
            const key = index + 1;
            return (
              <Menu.Item id={"000" + key} key={key} onClick={targetHandler}>
                <span id={"000" + key}>
                  {days[date.getDay()]}
                  <br />
                  {date.toLocaleDateString()}
                </span>
              </Menu.Item>
            );
          })}
        </Menu>
      </Header>
    </Layout>
  );
}

export default AppHeader;
