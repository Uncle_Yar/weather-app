import { combineReducers } from "redux";
import WeatherApp from "./WeatherApp";

const reducers = combineReducers({
  WeatherApp,
});

export default reducers;
