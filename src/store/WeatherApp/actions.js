import { SET_DATA, SET_TARGET } from "./types";

export const setData = (data) => ({
  type: SET_DATA,
  payload: data,
});

export const setTarget = (data) => ({
  type: SET_TARGET,
  payload: data,
});
