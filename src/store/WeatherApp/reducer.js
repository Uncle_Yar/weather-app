import { SET_DATA, SET_TARGET } from "./types";

const initialState = {
  data: [],
  target: "0000",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA: {
      return { ...state, data: action.payload };
    }
    case SET_TARGET: {
      return {
        ...state,
        target: action.payload,
      };
    }

    default:
      return state;
  }
};

export default reducer;
