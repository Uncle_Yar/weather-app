const getData = () => (state) => state.WeatherApp.data;

const isProductInCart = (article) => (state) =>
  !!state.WeatherApp.data.find((product) => product.article === article);
const selectors = {
  getData,
  isProductInCart,
};
export default selectors;
