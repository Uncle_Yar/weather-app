import { setData, setTarget } from "./actions";
import axios from "axios";

const fetchWeather = (lat, lon) => (dispatch) => {
  axios
    .post(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely&appid=7dc35104e5244ae340f07980f9f7f986&units=metric&lang=ua`
    )
    .then((res) => {
      axios
        .get(
          `https://eu1.locationiq.com/v1/reverse.php?key=pk.4d9fd1450692c50688e11b4f9ae6366f&lat=${lat}&lon=${lon}&format=json`
        )
        .then((result) => {
          let address = result.data.address;
          let data = { ...res.data, address };
          dispatch(setData(data));
        });
    });
};
const actions = { setData, setTarget, fetchWeather };

export default actions;
