import React, { useEffect } from "react";
import "./App.css";
import "antd/dist/antd.css";
import AppHeader from "./components/AppHeader/AppHeader";
import AppRoutes from "./routers/AppRoutes";
import { WeatherAppSelectors, WeatherAppOperations } from "./store/WeatherApp";
import { useDispatch, useSelector } from "react-redux";

const App = (props) => {
  const dispatch = useDispatch();
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(function (position) {
      if (position.coords)
        dispatch(
          WeatherAppOperations.fetchWeather(
            position.coords.latitude,
            position.coords.longitude
          )
        );
    });
  }, [dispatch]);

  const data = useSelector(WeatherAppSelectors.getData());
  return (
    <div className="App">
      {data.length === 0 ? (
        <div className="loader">Loading...</div>
      ) : (
        <div>
          <AppHeader data={data} />
          <AppRoutes data={data} />
        </div>
      )}
    </div>
  );
};

export default App;
