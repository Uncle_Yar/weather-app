export const getIconClassName = (weather) => {
  const weatherLC = weather.toLowerCase();
  if (weatherLC === "clouds") {
    return "cloud";
  } else if (weatherLC === "rain") {
    return "rain";
  }
  return "sun";
};
export const timeFormat = (date) => {
  let hours = date.getHours();
  let minutes = date.getMinutes();
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  return `${hours}:${minutes}`;
};
const funcs = { getIconClassName, timeFormat };
export default funcs;
