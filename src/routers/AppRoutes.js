import React from "react";
import { Route, Switch } from "react-router-dom";
import WeatherApp from "../pages/WeatherApp/WeatherApp";

function AppRoutes(props) {
  return (
    <Switch>
      <Route exact path="/weather-app">
        <WeatherApp data={props.data} />
      </Route>
    </Switch>
  );
}

export default AppRoutes;
